# Daniel Barrington
<daniel.barrington@gmail.com> | +447491930897 | 249 Caledonian Rd, N1 1ED, London, UK

## Front End Dev
ES6+ | Modern Frameworks | SASS | Git | Nodejs | Docker | Webpack

Front End focused dev with experience across Full Stack and DevOps. I'm as interested in build tool configs and CI/CD pipelines as much as slick designs and intuitive user interfaces. 
From working in small startup teams, I've been exposed to AWS and GCloud infrastructure, DB architecture, API design, user accessibility, wire framing and even logo design.
More recently I've had the opportunity to hone my frontend skills working on international customer facing sites as well as developing robust internal business user interfaces for enterprise grade software.

## Experience
**Front End Dev | Pancentric Digital**  
2017 - 2019 | London, UK  
Pancentric, a digital agency servicing multinational corporations and developing insurance products.
- Work across a variaty of disciplines including Java, Python | Django, ASP.NET | Sitecore & Umbraco
- Focus on modern frontend tech such as ES6+, SASS
- Update legacy codebases and architect new projects
- Refine build processes and CI/CD jobs

**Tech lead | Ace**  
2016 - 2017 | Dublin, Ireland  
Ace, an app to connect sporting event organisers and participants. Organisers can create events, fill out relevant details and collect signup fees through the app. Participants can search and filter upcoming events, view details and other participants, and register themselves for the event.
- Design SPA from front to backend
- Develop mobile first web app with Angularjs, Cordova, Ionic, Nodejs, MySQL

**Full Stack Dev | Lightbox**  
2014 | Dublin, Ireland  
Lightbox, an award winning digital agency providing clients with engaging web, mobile and social media solutions.
- Quickly pick up and contribute to existing projects
- Speccing and pricing new projects

**Front End Dev | Tomnod**  
2011 - 2013 | San Diego, California  
Tomnod, a crowdsourcing platform to analyse satellite imagery. User are tasked with identifying specific features, if there is consensus between users we can confidently analyse large areas of the globe. 
- Develop intuitive UI with Backbone, Underscore on top of a Slippy Map to allow users to easily identify points of interest
- Include aspects of gamification and positive user feedback to keep contributors engaged
- Help launch and grow a startup to acquisition

**Freelance Web Dev**  
2007 - 2011 | Dublin, Ireland  

**BSc Computer Science**  
2003 - 2007 | UCD, Dublin, Ireland

## Links

[Gitlab](https://gitlab.com/barro32)  
[Stackoverflow](https://stackoverflow.com/users/766966/barro32)